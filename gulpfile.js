var gulp          = require('gulp'),
    sass          = require('gulp-sass'),
    uglify        = require('gulp-uglify-es').default,
    autoprefixer  = require('gulp-autoprefixer'),
    browserSync   = require('browser-sync').create();

const config = {
  src: {
    html: 'src/**/*.html',
    php: 'src/**/*.php',
    styles: 'src/styles/**/*.+(css|scss|sass|less)',
    scripts: 'src/scripts/**/*.js',
    staticFiles: 'src/**/*.+(jpg|jpeg|png|gif|svg|ttf|otf|woff|eot|mp3|mp4|txt)'
  },
  build: {
    html: 'build/',
    php: 'build/',
    styles: 'build/styles',
    scripts: 'build/scripts',
    staticFiles: 'build/'
  },
  server: {
    proxy: 'lavash.dev'
    // server: 'build'
  }
};

gulp.task('build::php', buildPhp);
gulp.task('build::html', buildHtml);
gulp.task('build::styles', buildStyles);
gulp.task('build::scripts', buildScripts);
gulp.task('build::static-files', buildStaticFiles); // Audio and video files, images, fonts etc.

gulp.task('build', ['build::html', 'build::styles', 'build::scripts', 'build::static-files', 'build::php']);

gulp.task('start-server', startServer);

gulp.task('default', ['build', 'start-server']);

function buildHtml() {
  gulp.src(config.src.html)
    .pipe(gulp.dest(config.build.html))
    .pipe(browserSync.stream());
}
function buildPhp() {
  gulp.src(config.src.php)
    .pipe(gulp.dest(config.build.php))
    .pipe(browserSync.stream());
}
function buildStyles() {
  gulp.src(config.src.styles)
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .on('error', logError)
    .pipe(gulp.dest(config.build.styles))
    .pipe(browserSync.stream());
}
function buildScripts() {
  gulp.src(config.src.scripts)
    .pipe(uglify())
    .on('error', logError)
    .pipe(gulp.dest(config.build.scripts))
    .pipe(browserSync.stream());
}
function buildStaticFiles() {
  gulp.src(config.src.staticFiles)
    .pipe(gulp.dest(config.build.staticFiles))
    .pipe(browserSync.stream());
}

function startServer() {
  browserSync.init(config.server);

  gulp.watch(config.src.php, buildPhp);
  gulp.watch(config.src.html, buildHtml);
  gulp.watch(config.src.styles, buildStyles);
  gulp.watch(config.src.scripts, buildScripts);
  gulp.watch(config.src.staticFiles, buildStaticFiles);
}

function logError(e) {
  console.log(e);
}
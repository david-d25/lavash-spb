"use strict";

const ANIMATE_DELAY = 800;

window.addEventListener("load", () => {
  $(".main-slideshow-bg").vegas({
    slides: [
      { src: "img/slideshow/1.jpg" },
      { src: "img/slideshow/2.jpg" },
      { src: "img/slideshow/3.jpg" }
    ],
    overlay: "img/vegas-overlays/01.png",
    animation: 'random',
    timer: false
  });

  window.onscroll = () => {
    NavbarController.scroll();
    SlideShowController.scroll();
  };
  NavbarController.scroll();

  $(".navbar__trigger").click(toggleNavbarMenu);
  $(".navbar__link").click(hideNavbarMenu);

  $(".navbar").on("click", "a", function (event) {
    event.preventDefault();
    let id = $(this).attr('href');
    scrollTo(id);
  });

  $(".order__add-to-cart").click(() => {  // Добавить в корзину
    ShoppingCart.addGood("LAVASH");
    $(".cart-icon__items-count").text(ShoppingCart.getCardGoodsCount());
    $(".order__add-to-cart").text("Добавить ещё");
    updateOrderList();
  });

  $(".mainpage__order-button").click(() => {
    event.preventDefault();
    let id = $(".mainpage__order-button").attr('href');
    scrollTo(id);
  });

  $(".cart-icon").click((e) => {
    let cartIcon = $(".cart-icon");
    if (!cartIcon.hasClass("opened") && !$(e.target).is($(".cart-content__close-button"))) {
      cartIcon.addClass("opened");
      $("body").addClass('disabled-scroll');
      updateOrderList();
    }
  });

  $(".cart-content__close-button").click(() => {
    $(".cart-icon").removeClass("opened");
    $("body").removeClass('disabled-scroll');
  });

  $(".cart-content__order-button").click(() => {
    if (ShoppingCart.getCardGoodsCount() > 0)
      window.location.href = "submit-order.html";
  });

  initYandexMaps();
  initRecommendations();
  ShoppingCart.restoreCartFromCookie(true);
  $(".cart-icon__items-count").text(ShoppingCart.getCardGoodsCount());
});

function scrollTo(elem, offset) {
  if (!$(elem).offset()) return;
  offset = offset || 0;
  var top = $(elem).offset().top;
  if (top > document.body.offsetHeight - window.innerHeight)
    top = document.body.offsetHeight - window.innerHeight;
  $('body, html').animate({scrollTop: top + offset}, ANIMATE_DELAY, "easeInOutExpo");
  NavbarController.freezeScrollEvent(ANIMATE_DELAY);
}

function updateOrderList() {
  let target = $(".cart-content__list");
  let source = document.getElementById("shopping-cart-item").innerHTML;
  let template = Handlebars.compile(source);
  let cart = ShoppingCart.getCartGoods();
  target.empty();
  $(".cart-content__cost").text(`Итого: ${ShoppingCart.calculateCost()}`);
  if (ShoppingCart.getCardGoodsCount() > 0) {
    for (let id in cart)
      target.append(template({
        id: id,
        cost: ShoppingCart.AVAILABLE_GOODS[id].price * cart[id],
        name: ShoppingCart.AVAILABLE_GOODS[id].name,
        number: cart[id],
        price: ShoppingCart.AVAILABLE_GOODS[id].price
      }));
    $(".cart-content__order-item-remove-button").click((e) => {
      ShoppingCart.removeGood($(e.target).attr("data-id"));
      ShoppingCart.saveCartToCookie();
      updateOrderList();
      $(".cart-icon__items-count").text(ShoppingCart.getCardGoodsCount());
    });
    $(".cart-content__order-item-edit-button").click((e) => {
      $(`.cart-content__order-item[data-id='${$(e.target).attr("data-id")}']`).addClass('editing');
      $(`.cart-content__order-item[data-id='${$(e.target).attr("data-id")}'] .cart-content__order-item-edit-prompt-number`)
        .val(ShoppingCart.getCartGoods()[$(e.target).attr("data-id")]);
    });
    $(".cart-content__order-item-edit-prompt-ok").click((e) => {
      $(`.cart-content__order-item[data-id='${$(e.target).attr("data-id")}']`).removeClass('editing');
      let value = $(`.cart-content__order-item[data-id='${$(e.target).attr("data-id")}'] .cart-content__order-item-edit-prompt-number`).val();
      ShoppingCart.setNumberOfGood($(e.target).attr("data-id"), value);
      updateOrderList();
      $(".cart-icon__items-count").text(ShoppingCart.getCardGoodsCount());
    });
    $(".cart-content__order-item-edit-prompt-cancel").click((e) => {
      $(`.cart-content__order-item[data-id='${$(e.target).attr("data-id")}']`).removeClass('editing');
    });
    $(".cart-content__order-button").removeClass("disabled");
  } else {
    target.append("<div class='shopping-cart-empty'>Это ваша корзина, здесь появятся добавленные товары</div>");
    $(".cart-content__order-button").addClass("disabled");
  }
}

function initRecommendations() {
  $(".recommended__product").click(function(event) {
    if ($(event.target).hasClass('recommended__more-close-btn')) return;
    let target = event.currentTarget;
    if ($(target).hasClass('selected')) return;
    let allTargets = $(".recommended__product");
    for (let a = 0; a < allTargets.length; a++)
      if (allTargets[a] != target) $(allTargets[a]).addClass("hidden");
    $(target).addClass("selected");
    scrollTo(target, -70);
  });

  $(".recommended__more-close-btn").click(function() {
    $(".recommended__product").removeClass("hidden selected");
  });
}

function initYandexMaps() {
  ymaps.ready(() => {
    window.map = new ymaps.Map("payment__map", {
      center: [59.82531606, 30.30722650], 
      zoom: 16
    });
    map.geoObjects.add(new ymaps.Placemark([59.82531606, 30.30722650], {
      hintContent: "Мы здесь!"
    }));
  });

  window.onresize = () => {
    map.container.fitToViewport();
  }
}

function showHeaderContacts() {
  $("header").removeClass('hidden');
  $(".navbar-wrapper").removeClass('hidden-header');
}
function hideHeaderContacts() {
  $("header").addClass('hidden');
  $(".navbar-wrapper").addClass('hidden-header');
}

function toggleNavbarMenu() {
  $(".navbar").toggleClass('opened'); 
  $("body").toggleClass('disabled-scroll');
  $(".navbar__graphics").toggleClass('opened');
  hideHeaderContacts();
}
function hideNavbarMenu() {
  $(".navbar").removeClass('opened'); 
  $("body").removeClass('disabled-scroll');
  $(".navbar__graphics").removeClass('opened');
}

function onShipmentMethodClicked() {
  $(".payment__button--shipment").addClass('activated');
  $(".payment__button--pickup").removeClass('activated');
  $(".payment__shipment-wrapper").addClass('opened');
  $(".payment__pickup-wrapper").removeClass('opened');
}

function onPickupMethodClicked() {
  $(".payment__button--pickup").addClass('activated');
  $(".payment__button--shipment").removeClass('activated');
  $(".payment__pickup-wrapper").addClass('opened');
  $(".payment__shipment-wrapper").removeClass('opened');
  map.container.fitToViewport();
}

let NavbarController = {
  savedOffset: window.pageYOffset,
  frozen: false,
  targets: ['mainpage', 'features', 'payment', 'order'],
  targetBindings: [$('.navbar__link--mainpage'), $('.navbar__link--features'), $('.navbar__link--payment'), $('.navbar__link--order')],
  scroll: () => {
    if (!NavbarController.frozen && window.pageYOffset < NavbarController.savedOffset)
      showHeaderContacts();
    else
      hideHeaderContacts();
    NavbarController.savedOffset = window.pageYOffset;
    for (let a = 0; a < NavbarController.targets.length; a++) {
      let current = document.getElementById(NavbarController.targets[a]);
      let aim = window.pageYOffset + window.innerHeight / 2;
      if (aim > current.offsetTop && aim < current.offsetTop + current.offsetHeight)
        NavbarController.targetBindings[a].addClass('navbar__link--current-page');
      else
        NavbarController.targetBindings[a].removeClass('navbar__link--current-page');
    }
  },
  freezeScrollEvent: (freezeTime) => {
    NavbarController.frozen = true;
    setTimeout(() => {NavbarController.frozen = false}, freezeTime);
  }
};

let SlideShowController = {
  target: $(".main-slideshow-bg"),
  scroll: function() {
    this.target.css("transform", `translateY(${Math.round(window.pageYOffset/2)}px`);
  }
}
var orderInfo = {};

window.addEventListener("load", () => {
  ShoppingCart.restoreCartFromCookie();
  if (ShoppingCart.getCardGoodsCount() === 0)
    return document.location.replace("/");

  generateOrderList();
  $(".back-to-store-button").click(() => {
    window.location.href = "/";
  });

  $(".data-form__input").focus((e) => {
    $(e.target).removeClass("wrong-input");
  });

  $(".back-to-data-form").click(() => {
    $(".check-data").addClass("hidden");
    $(".data-form").removeClass("hidden");
  });

  $(".next-step-button").click(() => {
    if (checkInputs()) {
      $(".check-data").removeClass("hidden");
      $(".data-form").addClass("hidden");
      orderInfo = {
        fname: $("#input-fname").val(),
        sname: $("#input-sname").val(),
        mname: $("#input-mname").val(),
        phone: $("#input-phone").val(),
        address: $("#input-address").val(),
        comment: $("#input-comment").val(),
        cart: ShoppingCart.getCartGoods(),
        cost: ShoppingCart.calculateCost()
      };
      $(".check-data__name").text(`Вас зовут ${orderInfo.fname} ${orderInfo.mname} ${orderInfo.sname}`);
      $(".check-data__phone").text(`Ваш телефон: ${orderInfo.phone}`);
      $(".check-data__address").text(`Адрес: ${orderInfo.address}`);
      $(".check-data__comment").text(`Комментарий к заказу: ${orderInfo.comment}`);
    } else
      $('body, html').animate({scrollTop: 0}, 300, "easeInOutExpo");
  });

  $(".submit-order-button").click(() => {
    $.ajax({
      type: "POST",
      url: "process-order.php",
      data: orderInfo,
      complete: onSubmitCompleted,
      dataType: "json"
    });
  });
});

function onSubmitCompleted(data) {
  switch (data.status) {
    case 200:
      ShoppingCart.clearCart();
      document.location.replace("order-success.html");
      break;
    case 460:
      checkInputs(0); // fname error
      break;
    case 461:
      checkInputs(1); // sname error
      break;
    case 462:
      checkInputs(2); // phone error
      break;
    case 463:
      checkInputs(3); // address error
      break;
    default:
      document.location.replace("order-fail.html"); // Unspecified error
  }
}

function generateOrderList() {
  let target = $(".items-list");
  let source = document.getElementById("item-template").innerHTML;
  let template = Handlebars.compile(source);
  let cart = ShoppingCart.getCartGoods();
  $(".total-cost").text(`Итого: ${ShoppingCart.calculateCost()}`);
  for (let id in cart)
    target.append(template({
      cost: ShoppingCart.AVAILABLE_GOODS[id].price * cart[id],
      name: ShoppingCart.AVAILABLE_GOODS[id].name,
      number: cart[id],
      price: ShoppingCart.AVAILABLE_GOODS[id].price
    }));
}

function checkInputs(markErrorIndex) {
  let result = true;
  let fname = $("#input-fname");
  let sname = $("#input-sname");
  let phone = $("#input-phone");
  let address = $("#input-address");

  if (markErrorIndex !== null) {
    $(".check-data").addClass("hidden");
    $(".data-form").removeClass("hidden");
    $('body, html').animate({scrollTop: 0}, 300, "easeInOutExpo");

    if (markErrorIndex === 0)
      fname.addClass("wrong-input");
    else if (markErrorIndex === 1)
      sname.addClass("wrong-input");
    else if (markErrorIndex === 2)
      phone.addClass("wrong-input");
    else if (markErrorIndex === 3)
      address.addClass("wrong-input");
  }

  if (!/\S/.test(fname.val())) {
    result = false;
    fname.addClass("wrong-input");
  }
  if (!/\S/.test(sname.val())) {
    result = false;
    sname.addClass("wrong-input");
  }
  if (!/^[+]*[(]?[0-9]{1,3}[)]?[-\s./0-9]*$/g.test(phone.val())) {
    result = false;
    phone.addClass("wrong-input");
  }
  if (!/\S/.test(address.val())) {
    result = false;
    address.addClass("wrong-input");
  }

  return result;
}
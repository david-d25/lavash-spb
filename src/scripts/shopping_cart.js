(function() {
  "use strict";

  const CART_COOKIE_NAME = "shopping-cart";
  const CART_COOKIE_LIFETIME = 1000 * 60 * 60 * 24 * 30; // 30 days

  let cart = {};
  let ShoppingCart = {
    get MAX_NUMBER_OF_GOOD() { return 99; },
    get AVAILABLE_GOODS() {
      return {
        LAVASH: {
          price: 600,
          name: "Лаваш (30 шт.)"
        }
      }
    }
  };

  function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([.$?*|{}()\[\]\\\/+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }

  function setCookie(name, value, lifetime) {
    let expires = 0;
    if (lifetime) {
      let date = new Date();
      date.setTime(date.getTime() + lifetime);
      expires = date;
    }
    value = encodeURIComponent(value);
    document.cookie = `${name}=${value};expires=${expires}`;
  }

  ShoppingCart.addGood = function (id) {
    if (cart[id] && cart[id] < ShoppingCart.MAX_NUMBER_OF_GOOD)
      cart[id]++;
    else
      cart[id] = cart[id] || 1;
    ShoppingCart.saveCartToCookie();
  };

  ShoppingCart.clearCart = function() {
    cart = {};
    ShoppingCart.saveCartToCookie();
  };

  ShoppingCart.removeGood = function(id) {
    delete cart[id];
    ShoppingCart.saveCartToCookie();
  };

  ShoppingCart.setNumberOfGood = function(id, number) {
    if (+number >= 0 && +number <= ShoppingCart.MAX_NUMBER_OF_GOOD)
      cart[id] = Math.round(+number);
    ShoppingCart.saveCartToCookie();
  };

  ShoppingCart.getCartGoods = function() {
    return cart;
  };

  ShoppingCart.getCardGoodsCount = function() {
    let result = 0;
    for (let key in cart)
      result += +cart[key];
    return result;
  };

  ShoppingCart.calculateCost = function() {
    let result = 0;
    for (let key in cart)
      result += cart[key]*ShoppingCart.AVAILABLE_GOODS[key].price;
    return result;
  };

  ShoppingCart.saveCartToCookie = function() {
    setCookie(CART_COOKIE_NAME, JSON.stringify(ShoppingCart.getCartGoods()), CART_COOKIE_LIFETIME);
  };

  ShoppingCart.restoreCartFromCookie = function(updateLifetime) {
    cart = JSON.parse(getCookie(CART_COOKIE_NAME));
    if (updateLifetime)
      ShoppingCart.saveCartToCookie();
  };

  window.addEventListener("load", () => {
    window.ShoppingCart = ShoppingCart;
  });
})();
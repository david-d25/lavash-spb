<?php

$fname = stripslashes($_POST['fname']);
$sname = stripslashes($_POST['sname']);
$mname = stripslashes($_POST['mname']);
$phone = stripslashes($_POST['phone']);
$address = stripslashes($_POST['address']);
$comment = stripslashes($_POST['comment']);
$cart = $_POST['cart'];

$to = "david-d25@yandex.ru";
$subject = "Заказ на сайте";

$cartText = '';
foreach ($cart as $key => $value)
  $cartText .= "<xmp style=\"display: inline;\">$key</xmp>: <xmp style=\"display: inline;\">$value</xmp><br>";

$headers = '';
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=utf-8   \r\n";

$text = <<<TXT
<h1>Новый заказ:</h1>
<strong>Имя: </strong><xmp style="display: inline;">$fname $mname $sname</xmp><br>
<strong>Телефон: </strong><a href="tel:$phone"><xmp style="display: inline;">$phone</xmp></a><br>
<strong>Адрес: </strong><xmp style="display: inline;">$address</xmp><br>
<strong>Заказ (идентификаторы и количество):</strong><br>
$cartText
TXT;

if (ctype_space($fname) || $fname == '') {
  header("HTTP/1.1 460 First name error");
} else if (ctype_space($sname) || $sname == '') {
  header("HTTP/1.1 461 Second name error");
} else if (ctype_space($phone) || $phone == '' || preg_match("/^[0-9\-\(\)\/\+\s]*$/", $phone) == 0) {
  header("HTTP/1.1 462 Phone name error");
} else if (ctype_space($address) || $address == '') {
  header("HTTP/1.1 463 Address error");
} else {
  if (mail($to, $subject, $text, $headers))
    header("HTTP/1.1 200 OK");
  else
    header("HTTP/1.1 530 Can't send mail");
}